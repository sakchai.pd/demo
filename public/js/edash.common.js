/* Simple JavaScript Inheritance
* By John Resig http://ejohn.org/
* MIT Licensed.
**Begin Simple JavaScript Inheritance*/
(function(){var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;this.Class = function(){};
Class.extend=function(prop){var _super=this.prototype;initializing=true;var prototype=new this();initializing=false;
for(var name in prop){prototype[name]=typeof prop[name]=='function'&&typeof _super[name]=='function'&& fnTest.test(prop[name])?(function(name, fn){return function() {var tmp = this._super;this._super = _super[name];var ret = fn.apply(this, arguments);this._super = tmp;return ret;};})(name, prop[name]):prop[name];}
function Class(){if(!initializing&&this.init)this.init.apply(this, arguments);}
Class.prototype = prototype;
Class.prototype.constructor = Class;
Class.extend = arguments.callee;
return Class;};
})();
/*End Simple JavaScript Inheritance*/
/* Prototype */
String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}
String.prototype.rpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = str+padString;
    return str;
}
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

/* End Prototype */

$g={};
$g.queryString=function () {
  var query_string = {};
  var query = window.location.search.substring(1); 
  var vars = query.split("&"); 
  for (var i=0;i<vars.length && query!='';i++) {
	var pair = vars[i].split("=");
		// If first entry with this name
	if (typeof query_string[pair[0]] === "undefined") {
	  query_string[pair[0]] = decodeURIComponent(pair[1]);
		// If second entry with this name
	} else if (typeof query_string[pair[0]] === "string") {
	  var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
	  query_string[pair[0]] = arr;
		// If third or later entry with this name
	} else {
	  query_string[pair[0]].push(decodeURIComponent(pair[1]));
	}
  } 
    return query_string;
}();
$g.browser = (function(){
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|CriOS|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    } 
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!== null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!== null) M.splice(1, 1, tem[1]);
    return {
        "type":(M[0].replace('CriOS','chrome').toLowerCase()),
        "ver":(M[1].toLowerCase()),
        "mobile":(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)),
        "touch":('ontouchstart' in window || window.DocumentTouch && document instanceof window.DocumentTouch || navigator.maxTouchPoints > 0 || window.navigator.msMaxTouchPoints > 0),
        "svg":(document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Shape", "1.0") || false),
        "canvas":(!!document.createElement("canvas").getContext || false)};
})();
$g.screen = {"width":screen.width,"height":screen.height};
$g.display = {"width":window.innerWidth,"height":window.innerHeight}; 
$g.loadStatus = {"loadStatus": document.getElementById("load-status"),"total":0,"progress":0,"totalLoad":0};

function loadData(o){
 if(o){
    var method = (o.method || 'GET').toUpperCase();
    var url = (o.url || '');
    var cb = (o.success || function(){});
    var httpReq=typeof XMLHttpRequest!='undefined'?new XMLHttpRequest():new ActiveXObject('Microsoft.XMLHTTP');
    
    httpReq.onloadstart=function(e){ 
    };
	httpReq.onreadystatechange=function(){
	    if (httpReq.readyState == 4 && httpReq.status == 200){
            cb(httpReq.responseText,o);
	    }
	};
	httpReq.onprogress=function(e){ 
	    if($g.loadStatus){
		  if(document.getElementById('load-bar')){
	        document.getElementById('load-bar').style.width = Math.round(e.loaded/e.total*100).toString()+'%';
		  }
		  if(document.getElementById('load-progress')){
	        document.getElementById('load-progress').innerHTML=Math.round(e.loaded/e.total*100)+'%';
		  }
	    }
	};
    httpReq.ontimeout = function (e) {
	    if($g.loadStatus){
            document.getElementById('load-status').innerHTML = o.url+' is timeout';
	    }
        
    }
    httpReq.onloadend=function(e){
	    if($g.loadStatus){
          window.setTimeout('document.getElementById("loading").style.display="none"',1500);
	    }
    };
	httpReq.open('GET',url,true);
	httpReq.send();
 } else { throw new Error('Error: loadData(param) does not exist.') }
}
function checkType(o){
    var objectType = {
        "[object Object]":"OBJECT","[object Array]":"ARRAY","[object Function]":"FUNCTION",
        "[object String]":"STRING","[object Number]":"NUMBER","[object Boolean]":"BOOLEAN"};
    return objectType[Object.prototype.toString.call(o)];
}