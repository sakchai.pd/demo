$visualObject = {};
function map(p){
 return new (Class.extend({
    init: function(p){
      try{
       id = (p.id || "TempID");
       this.id = id;
       this.visualType = 'MAP';
       this.viewType = (p.viewType||'street');
       
       let lZoom = (p.zoom || 5.15);
       let lCenterPoint = [(p.lat || 13.165),(p.lng || 101.385)];
       let lZoomSnap = (p.zoomSnap || 0.25);

       this.map = L.map(id,{"center":lCenterPoint, "zoom":lZoom});
       L.control.scale({position:'bottomright',imperial:false}).addTo(this.map) /* add Scale to Map */;
	   this.map.zoomState = false;
	   this.map.addControl(new (L.Control.extend({ //Title Control
        options: {position: 'topleft'},
            onAdd: function (map) {
            var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-icon'); 
    	    container.style.backgroundColor = 'white';
	        container.style.width = '26px';
	        container.style.height = '26px';
	        container.style.cursor = 'pointer';
	        container.style.backgroundImage = 'url(images/'+(this.viewType && (this.viewType=='unidentified' || this.viewType=='street')?'street':'satellite')+'.png)';
	        container.onclick = function(){setTile(id)};
	    return container;
	   }
       }))());
       
       this.map.on('click', function(e) { mapClick(e) });
       this.map.on('moveend', function() { mapPan() });
       this.map.on('zoomend', function() { mapZoom});
       
       $visualObject[id] = this;
	   setTile(id);

      } catch(err) { /* Catch Error */
        console.log("Error :"+err);
      }
    }, /*end init fn*/
    
 }) /*end Class.extend*/ 
 )(p) /*end retuen new()*/
 }
 
function setTile(mapId){ 
    let mapObj = $visualObject[mapId];
    let tile = (mapObj.viewType === 'street' ? 'm,t' : 's,h') ;
	if( mapObj.map.hasLayer(mapObj.tile)){
      mapObj.map.removeLayer(mapObj.tile);
    }

    mapObj.tile = L.tileLayer('https://{s}.google.com/vt/lyrs='+tile+'&x={x}&y={y}&z={z}',
                            {maxZoom: 20,subdomains:['mt0','mt1','mt2','mt3'],attribution:'Map Data &copy; Google'});
    mapObj.tile.addTo(mapObj.map);    
	mapObj.viewType = (mapObj.viewType === 'satellite' ? 'street' : 'satellite');
}

 